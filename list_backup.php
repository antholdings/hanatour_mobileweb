<?
	$city=$_GET['city'];
	$startdate=$_GET['sd'];
	$enddate=$_GET['ed'];
	$room0=$_GET['rt0'];
	$room1=$_GET['rt1'];
	$room2=$_GET['rt2'];
	$room3=$_GET['rt3'];
	$room4=$_GET['rt4'];
	
	function get_content($url) {
	$agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)';
	$curlsession = curl_init ();
	$headers=array("Content-type: text/xml;charset=\"utf-8\"");
	 
	curl_setopt ($curlsession, CURLOPT_URL, $url);
	curl_setopt ($curlsession, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($curlsession, CURLOPT_USERAGENT, $agent);
	curl_setopt ($curlsession, CURLOPT_REFERER, "");
	curl_setopt ($curlsession, CURLOPT_CRLF, true);
	
	//curl_setopt ($curlsession, CURLOPT_TIMEOUT, 10);  //타임아웃 시간
	$buffer = curl_exec ($curlsession);
	$cinfo = curl_getinfo($curlsession);
	curl_close($curlsession);
	if ($cinfo['http_code'] != 200)
	{
		return "Error";
	}
		return $buffer;
	}
	if($city==null)$city="TYO";
	if($startdate==null)$startdate="20120925";
	if($enddate==null)$enddate="20120926";
	if($room0==null)$room0="1";
	if($room1==null)$room1="0";
	if($room2==null)$room2="0";
	if($room3==null)$room3="0";
	if($room4==null)$room4="0";
	$data="http://dev.cjworldis.com/MobileApp/AntMobile.do?method=getHotelSearch&reqStrXML=";
	$data.="<LODGE_GET_SEARCHED_ABR_LODGE_PRICE_LIST>				<SITE_CODE>C30636S000</SITE_CODE>			<LICENSE_KEY>2324917697166561873817697187381735016656183911700316656</LICENSE_KEY>			<SAL_SITE_CODE>C30636S001</SAL_SITE_CODE>";
	$data.="<CITY_CODE>$city</CITY_CODE>";
	$data.="<CHECKIN_DATE>$startdate</CHECKIN_DATE><CHECKOUT_DATE>$enddate</CHECKOUT_DATE>";
	$data.="<BED_TYPE><BED_SG>$room0</BED_SG><BED_DB>$room1</BED_DB><BED_TW>$room2</BED_TW><BED_TP>$room3</BED_TP><BED_QD>$room4</BED_QD></BED_TYPE>";
	$data.="<LODGE_CODE></LODGE_CODE>			<LODGE_NAME></LODGE_NAME>			<SAL_ECODE>E00031</SAL_ECODE>		</LODGE_GET_SEARCHED_ABR_LODGE_PRICE_LIST>	";

	$data=get_content($data);
 
	$data=iconv("EUC-KR","UTF-8",$data);
	$data=str_replace("&lt;","<",$data);
	
	$data=str_replace("\n", '', $data);
	$data=str_replace("	",'',$data);
	$data=str_replace("\r",'',$data);
	
	//echo $data;
	$xml2 = simplexml_load_string($data);
	//echo "data=".$xml2;
	$ri=$xml2->RESULT_INFO;
	
	$count=count($ri->RESULT_DATA);
	$currency_rate=$ri->EXCHANGE_RATE_INFO->attributes()->RATE;

	

?>
<!DOCTYPE html>
	<html lang="ko">
	<head>
		<meta charset=utf-8 />
		<meta name="viewport" content="width=640, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5, user-scalable=no">
		<title></title>
		<!--[if lt IE 9]><script src="./html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" type="text/css" href="./default.css">
		<link rel="stylesheet" type="text/css" href="./common.css">
		<link href='http://api.mobilis.co.kr/webfonts/css/?fontface=NanumGothicWeb' rel='stylesheet' type='text/css' />
		<link href='http://api.mobilis.co.kr/webfonts/css/?fontface=NanumGothicBoldWeb' rel='stylesheet' type='text/css' />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
	</head>
	<body>
		<header>
			<nav>
				<a href=""><img src="./images/select.jpg" alt="select" class="select left"/></a>
				<input type="text" class="search" value="검색"/>
				<a href=""><img src="./images/home.jpg" alt="home" class="home right"/></a>
			</nav>
		</header>
			
		<section id="hotel">
		<?
		for($i=0;$i<$count;$i++)
		{
			$target=$ri->RESULT_DATA[$i];
			//echo $target->LODGE_MASTER->LODGE_NAME."</br>";
			$imgno=$target->LODGE_MASTER->attributes()->IMG_NO;
			$contents=$target->LODGE_MASTER->CONTENT;
			$title=$target->LODGE_MASTER->LODGE_NAME;
			if(strlen($title)>20)	$title=mb_substr($title,0,20,"utf-8")." ...";
			if(strlen($contents)>50)	$contents=mb_substr($contents,0,50,"utf-8")." ...";
			$rate=$target->LODGE_MASTER->attributes()->REVIEW_GRADE;
			
			
			$room_target=$target->PRICE_INFO_LIST;
			$room_count=count($room_target->ROOM_INFO);
			$price_value=-1;
			for($j=0;$j<$room_count;$j++)
			{
				$price_target=$room_target->ROOM_INFO[$j];
				$price_count=count($price_target->PRICE_INFO);
				for($k=0;$k<$price_count;$k++)
				{
					if($price_target->PRICE_INFO[$k]->attributes()->TOTAL_SALE_PRICE < $price_value || $price_value<0)
					{
						$price_value=$price_target->PRICE_INFO[$k]->attributes()->TOTAL_SALE_PRICE;
						
					}
				}
			}
			//$imgno=$target->attributes();
			//echo $imgno;
			
			if($price_value>0)
			{
			$code=$target->LODGE_MASTER->attributes()->CODE;
			$link_url="detail.php?start_date=$startdate&end_date=$enddate&room0=$room0&room1=$room1&room2=$room2&room3=$room3&room4=$room4";
			$link_url.="&code=$code";
			$link_url.="&city=$city";
?>
			
			<ul>
			<a href="<?echo $link_url;?>" style="color: inherit;text-decoration:none;">
				<li class="img">
				<!--<img src="./images/listimg.jpg" alt="listimg">-->
				<img src=<?echo "http://tourall.co.kr/app/wsv/hotel/view_image.asp?company_code=C00002&obj_img=image1&img_no=".$imgno;?> alt="호텔사진" width="139px" height="139px">
				</li>
				<li class="title"><?echo $title;?></li>
				<li class="subtitle"><? ?> </li>
				<li class="description"><?echo $contents;?></li>
				<li class="price"><?echo (int)($price_value*$currency_rate);?>원~</li>
				<li class="star">
					<?	
					
					for($j=0;$j<$rate;$j++)						{						?>
							<img src="./images/star.jpg" alt="star">
					<?						}					
					for($j=5;$j>$rate;$j--)						{						?>
					
						<img src="./images/star_off.jpg" alt="staroff">
					<?	}					?>
					
				</li>
				<li class="more"><img src="./images/list_more.jpg" alt="list_more"></li>
				</a>
			</ul>
			
			<?
			
		}
		}
		?>
		<ul style="display:block; width:100%;height:100px; margin:0; padding:0;"></ul>
		</section>
	
		
		
		<footer>
			<div class="checkin">05.26<img src="./images/checkin.jpg" alt="checkin"/></div>
			<div class="checkout">05.30<img src="./images/checkout.jpg" alt="checkout"/></div>
			<div class="footer_person"><img src="./images/footer_person.jpg" alt="footer_person"/>1</div>
			<div class="footer_home"><img src="./images/footer_home.jpg" alt="footer_home"/>1</div>
		</footer>
		
	</body>
	</html>
<script>
	$('.rollmenu').hover(
		function () {
			var selected_id = $(this).attr('id');
			$('.rollmenu').each(function(){
				if($(this).attr('id')==selected_id){
					$(this).attr('src','./images/'+$(this).attr('id')+'_on.jpg');
					$('#'+$(this).attr('id')+'_detail').show();
				}else{
					$(this).attr('src','./images/'+$(this).attr('id')+'.jpg');
					$('#'+$(this).attr('id')+'_detail').hide();
				}
			});
		},
		function () {
		}
	);
</script>
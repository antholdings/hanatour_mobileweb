$(document).ready( function(){

	$("div.tabButton").click(function(){ // Controll style for tabButton
		if($(this).hasClass("selected")) { return; }
		else { 
			$("div.tabButton").removeClass("selected"); 
			$(this).addClass("selected");
			$("div.tabContents").css("display", "none");
				
			var tabId = $(this).attr("id");
			$("."+tabId).fadeIn("fast");
		}
	});
			
	$(".blueButton").mousedown(function() { $(this).addClass("blueButtonSelected"); }).mouseup(function() { $(this).removeClass("blueButtonSelected");});
	$("span.input > input, span.input > select").live("focus", function(){ $(this).addClass("focus"); }).live("focusout", function() {$(this).removeClass("focus");});
	
	$(".switch").click(function() { if($(this).hasClass("on")) { $(this).removeClass("on").addClass("off"); } else { $(this).addClass("on").removeClass("off"); } });
});


$(document).ready( function(){
	$("select#roomCount").change(function() {
		var roomCount = $("select#roomCount option:selected").val().substring(0, 1);
		
		$("ul#roomBaseInformation li").remove();
		for(var i=1;i<=roomCount;i++)
		{
			$("ul#roomBaseInformation").append("<li><span class=\"label\">객실"+i+"</span><span class=\"input\"><select class=\"people\" name=\"room"+i+"countAdult\" id=\"room"+i+"countAdult\"><option selected>성인 1명</option><option>성인 2명</option><option>성인 3명</option><option>성인 4명</option></select><select class=\"people\" name=\"room"+i+"countTeen\" id=\"room"+i+"countTeen\"><option selected>소아 0명</option><option>소아 1명</option><option>소아 2명</option><option>소아 3명</option></select></li>");
		}
		
		$("ul#teenAgeInformation > li").remove();
		
		
	}); // 방의 갯수에 따라 인원을 정할수 있는 select가 늘어나는 함수
	
	$("select.people").live("change", function() {
		var id = $(this).attr("id").valueOf().substring(0, 10);
		var type = $(this).val().substring(0, 2);
		var adultCount = $("select#"+id+"Adult").val().substring(5, 2).substring(0, 2)*1;
		var teenCount = $("select#"+id+"Teen").val().substring(5, 2).substring(0, 2)*1;
		
		if(type == "성인")
		{
			
			$("select#"+id+"Teen > option").remove();
			var max = 4-adultCount;
			if(!(max > teenCount)) teenCount = max; 
			
			for(var i=0;i<=max;i++)
			{
				if(i==teenCount)
					$("select#"+id+"Teen").append("<option selected>소아 "+i+"명</option>");
				else
					$("select#"+id+"Teen").append("<option>소아 "+i+"명</option>");
			}
		}
		else
		{	
			$("select#"+id+"Adult > option").remove();
			var max = 4-teenCount;
			if(!(max > adultCount)) adultCount = max; 
			
			for(var i=0;i<=max;i++)
			{
				if(i==adultCount)
					$("select#"+id+"Adult").append("<option selected>성인 "+i+"명</option>");
				else
					$("select#"+id+"Adult").append("<option>성인 "+i+"명</option>");
			}
		}
		
	});
	
	$("span.input > select.people:last-child").live("change", function() {
		var id = $(this).attr("id").valueOf().substring(0, 10);
		var location = id.substring(4,5);
		var teenCount = $("select#"+id+"Teen").val().substring(5, 2).substring(0, 2)*1;
		
		if($("li#TeenAge"+location).length) $("li#TeenAge"+location).remove();
		
		switch(teenCount)
		{
			case 1 : $("ul#teenAgeInformation").append("<li id=\"TeenAge"+location+"\"><span class=\"label\">객실"+location+"</span><span class=\"input single\"><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select></span></li>"); break;
			case 2 : $("ul#teenAgeInformation").append("<li id=\"TeenAge"+location+"\"><span class=\"label\">객실"+location+"</span><span class=\"input double\"><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select></span></li>"); break;
			case 3 : $("ul#teenAgeInformation").append("<li id=\"TeenAge"+location+"\"><span class=\"label\">객실"+location+"</span><span class=\"input triple\"><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select></span></li>"); break;
			case 4 : $("ul#teenAgeInformation").append("<li id=\"TeenAge"+location+"\"><span class=\"label\">객실"+location+"</span><span class=\"input quadruple\"><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select><select><option selected>1세</option><option>2세</option><option>3세</option><option>4세</option><option>5세</option><option>6세</option><option>7세</option></select></span></li>"); break;
		}
		
		var li = $('ul#teenAgeInformation > li');            
                        
        li.sort(function(a, b){
        	return $(a).attr("id") > $(b).attr("id") ? 1 : -1;
        });
           
        
                
	});
	
});


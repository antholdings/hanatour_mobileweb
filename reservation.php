<?
session_start();	
	//LIST에서 넘겨오는 값 GET
	$start_date=$_GET['start_date'];
	$end_date=$_GET['end_date'];
	$single_room=$_GET['room0'];
	$double_room=$_GET['room1'];
	$twin_room=$_GET['room2'];
	$triple_room=$_GET['room3'];
	$quad_room=$_GET['room4'];
	$code=$_GET['code'];
	$city=$_GET['city'];
		$room_type=$_GET['rt'];
	$room_option=$_GET['ro'];
		
	//고정값.
	$sal_ecode="E00031";
	$license_key="2324917697166561873817697187381735016656183911700316656";//개발용.
	$site_code="C30636S000";
	$sal_site_code="C30636S001";
	$url="http://dev.cjworldis.com/MobileApp/AntMobile.do?method=getHotelSearch&reqStrXML=";
	$url.="<LODGE_GET_SEARCHED_ABR_LODGE_PRICE_LIST><SITE_CODE>$site_code</SITE_CODE><LICENSE_KEY>$license_key</LICENSE_KEY><SAL_SITE_CODE>$sal_site_code</SAL_SITE_CODE><CITY_CODE>$city</CITY_CODE><CHECKIN_DATE>$start_date</CHECKIN_DATE><CHECKOUT_DATE>$end_date</CHECKOUT_DATE><BED_TYPE><BED_SG>$single_room</BED_SG><BED_DB>$double_room</BED_DB><BED_TW>$twin_room</BED_TW><BED_TP>$triple_room</BED_TP><BED_QD>$quad_room</BED_QD></BED_TYPE><LODGE_CODE>$code</LODGE_CODE><LODGE_NAME></LODGE_NAME><SAL_ECODE>$sal_ecode</SAL_ECODE></LODGE_GET_SEARCHED_ABR_LODGE_PRICE_LIST>";
	//내용 불러오기.
	function get_Content($_url){
			// 핸들 생성
			$cURL = curl_init(); 
			// 대상 URL 설정
			curl_setopt($cURL, CURLOPT_URL,$_url);
			curl_setopt($cURL, CURLOPT_HEADER,0);
			//1->DIRECT RETURN 0->RESULT RETURN
			curl_setopt($cURL, CURLOPT_RETURNTRANSFER,1); 
			// cURL 실행
			$data = curl_exec($cURL);
			// 핸들 닫기
			curl_close($cURL);

			return $data;	
		}
		//
		$Result = get_Content($url);
		$Result = iconv("EUC-KR", "UTF-8",$Result);
		$Result = str_replace("&lt;","<",$Result);
		$Result = str_replace("\n","",$Result);
		$Result = str_replace("\r","",$Result);
		$Result_xml=simplexml_load_string($Result);

		$xml_country=$Result_xml->RESULT_INFO->CITY_INFO;
		$xml_data=$Result_xml->RESULT_INFO->RESULT_DATA;
		$currency_rate=$Result_xml->RESULT_INFO->EXCHANGE_RATE_INFO->attributes()->RATE;
		$currency_date=$Result_xml->RESULT_INFO->EXCHANGE_RATE_INFO->attributes()->ISSUE_DATE;
		$roominfo=$xml_data->PRICE_INFO_LIST->ROOM_INFO;
		$target_room=null;
		
		$imgno = $xml_data->LODGE_MASTER->attributes()->IMG_NO;
		
		for($i=0;$i<count($roominfo);$i++)
		{
		
			if($roominfo[$i]->attributes()->ROOM_TYPE==$room_type && $roominfo[$i]->attributes()->ROOM_OPTION_CODE==$room_option)
			{
				$target_room=$roominfo[$i];	
			}
		}
		$price=$target_room->PRICE_INFO->attributes()->TOTAL_SALE_PRICE*$currency_rate;
		//호텔 이름(영문)
		$title = $xml_data->LODGE_MASTER->LODGE_NAME;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/function.js"></script>
		<link rel="stylesheet" href="assets/css/reset.css" />
		<link rel="stylesheet" href="assets/css/layout.css" />
		<title>예약하기</title>
		<script type="text/javascript">
			$(document).ready(function() { $("div.st").click(function() {
					if($(this).css("background-position").valueOf()=="0px 0px") { $(this).css("background-position", "0px -30px"); }
					else { $(this).css("background-position", "0px 0px"); }
				});
			});
		</script>
	</head>
	<body>
		<?php 

	
		?>
		<div id="bodyWrapper">
			<div id="header">
				<a onclick="javascript:history.back(-1)"><img id="back" src="assets/img/button.png" /></a>
				<h3>예약하기</h3>
			</div>		
			<div class="hotelDetail container">
				<span class="thumbnail">
				<!--<img src="assets/img/grand.png" />-->
				<img src=<?
				echo "http://tourall.co.kr/app/wsv/hotel/view_image.asp?company_code=C00002&obj_img=image1&img_no=".$imgno; ?>
				alt="hotel">
				</span>
				<span class="info">
					<span><h5><?echo $title;?></h5></span>
					<span><?echo $xml_data->LODGE_MASTER->attributes()->GRADE."성"?></span>
					
				</span>
			</div>
					
			<div class="tabContents container">
				<h5><?echo $target_room->attributes()->ROOM_TYPE_NAME?></h5>
				<ul>
					<li><span class="label">투숙기간</span><span class="input"><?=$start_date?>~<?=$end_date?></span></li>
					<li><span class="label">객실수</span>
						<?
						echo $single_room+$twin_room+$double_room+$triple_room+$quad_room;
						?>
					
					</li>
					<li style="height: 30px;">
						<span class="label">배드타입</span>
						<?
						if($single_room)							echo "싱글:".$single_room."  ";
						if($twin_room)							echo "트윈:".$twin_room."  ";
						if($double_room)							echo "더블:".$double_room."  ";
						if($triple_room)							echo "3인실:".$triple_room."  ";
						if($quad_room)							echo "4인실:".$quad_room."  ";
					
					?>
					
						</li>
						
					
					<li><span class="label">합계</span><span class="input"><?echo $price."원";?></span></li>
					<?
					$reserv_url="reservContinue.php?";//예약 url.
			$reserv_url.="start_date=$start_date&end_date=$end_date&room0=$single_room&room1=$double_room&room2=$twin_room&room3=$triple_room&room4=$quad_room";
			$reserv_url.="&code=$code";
			$reserv_url.="&city=$city";
			$reserv_url.="&rt=".$room_type;
			$reserv_url.="&ro=".$room_option;
		
					?>
					<li style="padding: 5px 10px 5px 10px;"><span><a href="<?echo $reserv_url; ?>"><input type="submit" class="blueButton" value="예약하기" /></a></span></li>
				</ul>
				
				<h5>상품 세부조건 및 취소조건</h5>
				<ul>
					<li>- 예약기간 : 2011년 6월 16일 ~ 22일<br />- 숙박기간 : 2011년 7월 8일 ~ 8월 31일<br />-슈페리어 객실 1박<br />- 1~2인 기준</li>
					<li>- 그랜드앰배서더서울 호텔의 정식명칭은 "Grand Ambassador Seoul associated with Pullman"입니다.<br /><br />[공통적용사항]</li>
					<li>상품사진 : <br />침대정보 : <br /><br />[호텔이용시 주의사항]</li>
				</ul>
			</div>
		</div>
		<?php //include_once('footer.php'); ?>
	</body>
</html>

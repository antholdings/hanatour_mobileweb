<?php
session_start();	
	//LIST에서 넘겨오는 값 GET
	$start_date=$_GET['start_date'];
	$end_date=$_GET['end_date'];
	$single_room=$_GET['room0'];
	$double_room=$_GET['room1'];
	$twin_room=$_GET['room2'];
	$triple_room=$_GET['room3'];
	$quad_room=$_GET['room4'];
	$code=$_GET['code'];
	$city=$_GET['city'];

	//테스트용으로 나중에 지우세요. 예외처리 안했어요 ㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋ
	if($city==null)
		$city="TYO";
	if($start_date==null)
		$start_date="20120930";
	if($end_date==null)
		$end_date="20121001";
	if($code==null)
		$code="H00098";
	if($single_room==null)
		$single_room="1";
	if($double_room==null)
		$double_room="0";
	if($twin_room==null)
		$twin_room="0";
	if($triple_room==null)
		$triple_room="0";
	if($quad_room==null)
		$quad_room="0";
	
	//고정값.
	$sal_ecode="E00031";
	$license_key="2324917697166561873817697187381735016656183911700316656";//개발용.
	$license_op_key="2324917697166561873817697187381735016656183911700317003";//운영용.
	$site_code="C30636S000";
	$company_code="C00002";
	$sal_site_code="C30636S001";
	
	//호텔 서치 url
	$url="http://dev.cjworldis.com/MobileApp/AntMobile.do?method=getHotelSearch&reqStrXML=";
	$url.="<LODGE_GET_SEARCHED_ABR_LODGE_PRICE_LIST><SITE_CODE>$site_code</SITE_CODE><LICENSE_KEY>$license_key</LICENSE_KEY><SAL_SITE_CODE>$sal_site_code</SAL_SITE_CODE><CITY_CODE>$city</CITY_CODE><CHECKIN_DATE>$start_date</CHECKIN_DATE><CHECKOUT_DATE>$end_date</CHECKOUT_DATE><BED_TYPE><BED_SG>$single_room</BED_SG><BED_DB>$double_room</BED_DB><BED_TW>$twin_room</BED_TW><BED_TP>$triple_room</BED_TP><BED_QD>$quad_room</BED_QD></BED_TYPE><LODGE_CODE>$code</LODGE_CODE><LODGE_NAME></LODGE_NAME><SAL_ECODE>$sal_ecode</SAL_ECODE></LODGE_GET_SEARCHED_ABR_LODGE_PRICE_LIST>";
	
	//디테일 url
	$detail_url="http://dev.cjworldis.com/MobileApp/AntMobile.do?method=getHotelDtlSearch&reqStrXML=";
	$detail_url.="<LODGE_GET_ABR_LODGE_INFO><SITE_CODE>$site_code</SITE_CODE><LICENSE_KEY>$license_op_key</LICENSE_KEY><SUP_COMPANY_CODE>$company_code</SUP_COMPANY_CODE><LODGE_CODE>$code</LODGE_CODE></LODGE_GET_ABR_LODGE_INFO>";
	
	//내용 불러오기.
	function get_Content($_url){
			// 핸들 생성
			$cURL = curl_init(); 
			// 대상 URL 설정
			curl_setopt($cURL, CURLOPT_URL,$_url);
			curl_setopt($cURL, CURLOPT_HEADER,0);
			//1->DIRECT RETURN 0->RESULT RETURN
			curl_setopt($cURL, CURLOPT_RETURNTRANSFER,1); 
			// cURL 실행
			$data = curl_exec($cURL);
			// 핸들 닫기
			curl_close($cURL);

			return $data;	
		}
		//
		$Result = get_Content($url);
		$Result = iconv("EUC-KR", "UTF-8",$Result);
		$Result = str_replace("&lt;","<",$Result);
		$Result = str_replace("\n","",$Result);
		$Result = str_replace("\r","",$Result);
		$Result_xml=simplexml_load_string($Result);

		$xml_country=$Result_xml->RESULT_INFO->CITY_INFO;
		$xml_data=$Result_xml->RESULT_INFO->RESULT_DATA;
		$currency_rate=$Result_xml->RESULT_INFO->EXCHANGE_RATE_INFO->attributes()->RATE;
		$currency_data=$Result_xml->RESULT_INFO->EXCHANGE_RATE_INFO->attributes()->ISSUE_DATE;
		//호텔 이름(영문)
		$title = $xml_data->LODGE_MASTER->LODGE_NAME;
		
		//호텔 상세정보
		$Detail_result = get_Content($detail_url);
		$Detail_result = iconv("EUC-KR", "UTF-8",$Detail_result);
		$Detail_result = str_replace("&lt;","<",$Detail_result);
		$Detail_result = str_replace("\n","",$Detail_result);
		$Detail_result = str_replace("▣","<br />▣",$Detail_result);
		$Detail_result = str_replace("\r","",$Detail_result);
		$Detail_xml=simplexml_load_string($Detail_result);
		$xml_detail = $Detail_xml->RESULT_INFO->RESULT_DATA;
		//echo "title=".$xml_detail->LODGE_KNAME;
?>

<!DOCTYPE html>
	<html lang="ko">
	<head>
	
		<style type="text/css">
		#imglist ul,#hotel li{display:inline;list-style:none;}
		#imglist ul{float:left;padding:15px 0;height:10px;height:100%;border-bottom:1px solid #bcbcbc; width:100%;}

		#imglist .img{float:center;width:100%;height:100%;}
		#imglist .img img{border:3px solid #7b7b7b;}
		</style>
		
		<meta charset=utf-8 />
		<meta name="viewport" content="width=640, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5, user-scalable=no">

		<title></title>
		<!--[if lt IE 9]><script src="./html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" type="text/css" href="./default.css">
		<link rel="stylesheet" type="text/css" href="./common.css">
		<link href='http://api.mobilis.co.kr/webfonts/css/?fontface=NanumGothicWeb' rel='stylesheet' type='text/css' />
		<link href='http://api.mobilis.co.kr/webfonts/css/?fontface=NanumGothicBoldWeb' rel='stylesheet' type='text/css' />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
		<!--google map 따로 빼셔도 됩니다~ㅋ -->
		<script type="text/javascript">
		var map;
		var infoWindow; // infoWindow Object를 담을 변수
		var marker; // Marker Object를 담을 변수
	    function initialize() {
	    	var lat=<? echo $xml_data->LODGE_MASTER->attributes()->LATITUDE; ?>;
	    	var lon=<? echo $xml_data->LODGE_MASTER->attributes()->LONGITUDE ?>;
	    	
		    var latlng = new google.maps.LatLng(lat,lon);
		    var myOptions = {
			    zoom: 8,
			    center: latlng,
			    mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        marker = new google.maps.Marker({
	     	position: latlng,
	     	title:"hotel"
	     	});
	     	
	     	// To add the marker to the map, call setMap();
	     	marker.setMap(map);
	     	google.maps.event.addListener(marker, 'click', ShowInfoWindow);
	     	infoWindow = new google.maps.InfoWindow(); //infoWindow 변수를 InfoWindow class 형식으로 할당 
	     
	     //footer 초기화
	     //initBottomMenu(); 
	    }
	    function ShowInfoWindow(event) {

		    infoWindow.setContent("<? echo $title ?>");
		    // setContent에 들어갈 HTML 코드는 줄바꾸기(enter)하지 말고 한 줄로 이어서 써야함

		    infoWindow.setPosition (event.latLng);

		    infoWindow.open (map);
		}
		function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=initialize";
  document.body.appendChild(script);
}
		</script>
	</head>
	<body onload="initBottomMenu();">
		<header>
			<nav>
			
			

				<a href="javascript:history.back()"><img src="./images/back.jpg" alt="back" class="back left"></a>
				<a href=""><img src="./images/doc.jpg" alt="doc" class="doc right"></a>
				<a href=""><img src="./images/home.jpg" alt="home" class="home right"></a>
			</nav>
		</header>
		<section id="content">
			<div class="title">
				<h1><img src="./images/best.jpg" alt="best" class="best icon">
				<? echo $title;?></h1>
				<h2><? echo $xml_detail->LODGE_KNAME; ?>
					<span class="star">
					<?
						$grade_rate=$xml_data->LODGE_MASTER->attributes()->REVIEW_GRADE;
						for($i=0;$i<$grade_rate;$i++){
					?>
						<img src="./images/star.jpg" alt="star">
					<?
						}
						for($i=5;$i>$grade_rate;$i--){						
					?>
						<img src="./images/star_off.jpg" alt="staroff">
					<? } ?>
					</span>
				</h2>
			</div>
			<div class="info">
				<?
				$imgno = $xml_data->LODGE_MASTER->attributes()->IMG_NO;
				?>
				<img src=<?
				echo "http://tourall.co.kr/app/wsv/hotel/view_image.asp?company_code=C00002&obj_img=image1&img_no=".$imgno; ?>
				alt="hotel">
				<?
				$country_code = $xml_country->attributes()->COUNTRY_CODE;
				$country_kname = $xml_country->attributes()->COUNTRY_KNAME;
				$country_ename = $xml_country->attributes()->COUNTRY_ENAME;
				$city_code = $xml_country->attributes()->CITY_CODE;
				$city_kname = $xml_country->attributes()->CITY_KNAME;
				$city_ename = $xml_country->attributes()->CITY_ENAME;
				?>
				<div class="address"><? echo "$city_kname($city_code), $country_kname($city_ename, $country_ename)" ?><div>
				<!--정보 없음
				<div class="address detail">
				2-14-3 NAGATO-CHO CHIYODA-KU TOKYO 100-0014 JAPAN</div>-->
				<div class="address detail">
				<? echo $xml_detail->ADDRESS ?></div>
				<ul>
					<li><? echo $xml_detail->LOCATION.", ".$xml_detail->LOCATION_DESC ?></li>
					<!--정보 없음-->
					<li><? echo "TEL : ".$xml_detail->TEL." / FAX : ".$xml_detail->FAX ?></li>
				</ul>
			</div>
		</section>
		<section id="reservation">
		<?
			$room_target=$xml_data->PRICE_INFO_LIST;
			$room_count=count($room_target->ROOM_INFO);
			$reserv_url="http://www.naver.com";//예약 url.
			
	
	
			for($j=0;$j<$room_count;$j++)
			{
				$room_info=$room_target->ROOM_INFO[$j];
				$room_title=$room_info->attributes()->ROOM_TYPE_NAME;
				$price=$room_info->PRICE_INFO->attributes()->TOTAL_SALE_PRICE;
				$meal=$room_info->attributes()->ROOM_OPTION_CODE;
				
			
			
			
		
		
		$product="<PRODUCT><GOOD_TYPE_CD>L</GOOD_TYPE_CD>";
		$product.="<COUNTRY_CODE>JP</COUNTRY_CODE>";
		$product.="<CITY_CODE>".$city."</CITY_CODE>";
		$product.="<CHECKIN_DATE>".$start_date."</CHECKIN_DATE>";
		$product.="<CHECKOUT_DATE>".$end_date."</CHECKOUT_DATE>";
		$product.="<BED_TYPE>"."1"."</BED_TYPE>";
		$product.="<BED_SG>".$single_room."</BED_SG>";
		$product.="<BED_DB>".$single_room."</BED_DB>";
		$product.="<BED_TW>".$single_room."</BED_TW>";
		$product.="<BED_TP>".$single_room."</BED_TP>";
		$product.="<BED_QD>".$single_room."</BED_QD>";
		$product.="<LODGE_CODE>".$xml_data->LODGE_MASTER->attributes()->CODE."</LODGE_CODE>";
		$product.="<LODGE_NAME>".$xml_data->LODGE_MASTER->LODGE_NAME."</LODGE_NAME>";
		$product.="<COMP_CODE>".$room_info->PRICE_INFO->attributes()->COMP_CODE."</COMP_CODE>";
		$product.="<CRS>".$room_info->PRICE_INFO->attributes()->CRS."</CRS>";
		$product.="<RATE>".$currency_rate."</RATE>";
		$product.="<ISSUE_DATE>".$currency_data."</ISSUE_DATE>";
		$product.="<ROOM_TYPE>".$room_info->PRICE_INFO->attributes()->ROOM_TYPE."</ROOM_TYPE>";
		$product.="<ROOM_OPTION_CODE>".$room_info->PRICE_INFO->attributes()->ROOM_OPTION_CODE."<ROOM_OPTION_CODE>";
		$product.="<TOTAL_SALE_PRICE>".(int)($price*$currency_rate)."</TOTAL_SALE_PRICE>";
	$product.="</PRODUCT>	";
		
		$product = str_replace("<","&lt;",$product);
		$product = str_replace(">","&gt;",$product);
	
		$reserv_url="reservation.php?";//예약 url.
			$reserv_url.="start_date=$start_date&end_date=$end_date&room0=$single_room&room1=$double_room&room2=$twin_room&room3=$triple_room&room4=$quad_room";
			$reserv_url.="&code=$code";
			$reserv_url.="&city=$city";
			$reserv_url.="&rt=".$room_info->attributes()->ROOM_TYPE;
			$reserv_url.="&ro=".$room_info->attributes()->ROOM_OPTION_CODE;
			
			
		 ?>
		 	<? if($j!=$room_count-1){ ?>
			<dl>
			<? } else{ ?>
			<dl class="last">
			<? } ?>
				<a href=<? echo $reserv_url; ?> style="color: inherit;">
				<dt><? echo $room_title ?></dt>
				<dd>
					<ul>
						<? if($meal==1){ ?>
						<li class="meal"><img src="./images/mealoff.jpg" alt="meal"></li>
						<? } else { ?>
						<li class="meal"><img src="./images/meal.jpg" alt="meal"></li>
						<? }?>
						<li class="price"><? echo (int)($price*$currency_rate); ?>원</li>
						<li class="more"><a href=""><img src="./images/more.jpg" alt="more"></a></li>
					</ul>
				</dd>
				</a>
			</dl>
		<? } ?>
		</section>
		<nav class="footer">
			<ul>
				<li class="aleft"><img src="./images/hotel_info_on.jpg" alt="hotel_info" id="hotel_info" class="rollmenu"></li>
				<li class="acenter"><img src="./images/hotel_pic.jpg" alt="hotel_pic" id="hotel_pic" class="rollmenu"></li>
				<li class="aright"><img src="./images/hotel_map.jpg" alt="hotel_map" id="hotel_map" class="rollmenu"></li>
			</ul>
		</nav>
		<!-- nav item이 강제로 한번 셀렉트되어야 할듯. 아래 스크립트 구현은 그게 안되있어서 최초에 모든 페이지가 다 show()가 됩니다.
		시간나시면 고치시고 아니면 제가 고칠께요.
		-->
		<section id="detail">
			<div class="scrollwrap">
				<span id="hotel_info_detail">
				<? 
				$addInfoXml=$xml_detail->ADD_INFO_LIST;
				$addInfo_count= count($addInfoXml->ADD_INFO); 
				for($i=0; $i<$addInfo_count;$i++){
					$info_xml=$addInfoXml->ADD_INFO[$i];
					echo $info_xml->TITLE."<br />";
					echo $info_xml->CONTENT."<br /><br />";
				}
				?>
				</span>
				<span id="hotel_pic_detail">
				<section id="imglist">
				<? for($i=0; $i<$addInfo_count;$i++){
					$info_xml=$addInfoXml->ADD_INFO[$i]->IMAGE_LIST;
					$imageList_count=count($info_xml->IMAGE_INFO);
					for($j=0; $j<$imageList_count; $j++){
					$imageinfo=$info_xml->IMAGE_INFO[$j];
					
				?>
				<ul>
				<li class="img">
				<!--<img src="./images/listimg.jpg" alt="listimg">-->
				<img src=
				<? 
				echo "http://tourall.co.kr/app/wsv/hotel/view_image.asp?company_code=C00002&obj_img=image1&img_no=".
				$imageinfo->IMG_NO; ?> 
				alt="호텔사진" width="100%" height="100%">
				</li>
				</ul>
				<? 		
					}
				}
				?>
				</section>
				</span>
				<span id="hotel_map_detail">
					<div id="map_canvas" style="width:100%; height:100%"></div>
				</span>
			</div>
		</section>
	</body>
	</html>
<script>
	$('.rollmenu').hover(
		function () {
			var selected_id = $(this).attr('id');
			$('.rollmenu').each(function(){
				if($(this).attr('id')==selected_id){
					$(this).attr('src','./images/'+$(this).attr('id')+'_on.jpg');
					$('#'+$(this).attr('id')+'_detail').show();
					var str=$(this).attr('id')+'_detail';
					if(str=='hotel_map_detail'){
						loadScript();
					}
				}else{
					$(this).attr('src','./images/'+$(this).attr('id')+'.jpg');
					$('#'+$(this).attr('id')+'_detail').hide();
				}
			});
		},
		function () {
			
		}
	);
	function initBottomMenu() {
			//document.getElementById('hotel_info_detail').focus();
			$('#hotel_info_detail').show();
			$('#hotel_pic_detail').hide();
			$('#hotel_map_detail').hide();
			
		}
	//initBottomMenu();
</script>
